//
//  APQXSwiftOandaAPITests.swift
//  APQXSwiftOandaAPITests
//
//  Created by Bonifatio Hartono on 4/1/16.
//  Copyright © 2016 Bonifatio Hartono. All rights reserved.
//

import XCTest
@testable import APQXSwiftOandaAPI
import APQXCSSP
class APQXSwiftOandaAPITests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAPQXSwiftOandaAPI() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        enum testCase : String{
            case GetAccounts = "Get accounts"
        }
        enum testSeqs : String {
            case CURLComponents = "Construct URL Components"
        }
        let X = APQX🚧App()
        /// Case Registry
        APQX🚧Case(desc: testCase.GetAccounts.rawValue).load(X)
        APQX🚧Case(desc: "Get account information").load(X)
        APQX🚧Case(desc: "Get instrument list").load(X)
        APQX🚧Case(desc: "Get current prices - Polling").load(X)
        APQX🚧Case(desc: "Get current prices - Streaming").load(X)
        APQX🚧Case(desc: "Retrieve instrument history").load(X)
        APQX🚧Case(desc: "Get orders").load(X)
        APQX🚧Case(desc: "POST orders").load(X)
        APQX🚧Case(desc: "Get information for an order").load(X)
        APQX🚧Case(desc: "Modify an existing order").load(X)
        APQX🚧Case(desc: "Close an order").load(X)
        APQX🚧Case(desc: "Get list of open trades").load(X)
        APQX🚧Case(desc: "Get information on a specific trade").load(X)
        APQX🚧Case(desc: "Modify an existing trade").load(X)
        APQX🚧Case(desc: "Close a trade").load(X)
        APQX🚧Case(desc: "Get a list of all open positions").load(X)
        APQX🚧Case(desc: "Get position for an instrument").load(X)
        APQX🚧Case(desc: "Close an existing position").load(X)
        APQX🚧Case(desc: "Get transaction history").load(X)
        APQX🚧Case(desc: "Get information for a transaction").load(X)
        APQX🚧Case(desc: "Get full account history").load(X)
        APQX🚧Case(desc: "Get events  (Streaming)").load(X)
        APQX🚧Case(desc: "Calendar").load(X)
        APQX🚧Case(desc: "Historical position ratios").load(X)
        APQX🚧Case(desc: "Spreads").load(X)
        APQX🚧Case(desc: "Commitment of Traders").load(X)
        APQX🚧Case(desc: "Orderbook").load(X)
        APQX🚧Case(desc: "Autochartist Patterns").load(X)

        
        APQX🚧Sequence(desc: testSeqs.CURLComponents.rawValue).load(X)
        APQX🚧Sequence(desc: "Construct URLRequest").load(X)
        /** Solution */
        APQX🚧Solution(inCase: testCase.GetAccounts.rawValue, inSequence: testSeqs.CURLComponents.rawValue, inParams: []).formula { (inParam) in
            NSURLComponents(environment: OandaEnvironment.FXTradePractice, basePath: OandaPollingBasePath.Accounts).URL!.absoluteString
            }.load(X, desc: "NSURLComponents(environment: OandaEnvironment.FXTradePractice, basePath: OandaPollingBasePath.Accounts).URL")
//        XCTAssertEqual(aTestResult?.absoluteString, "https://api-fxpractice.oanda.com/v1/accounts")
        
        //////////////
        /* End of Solution */
        X.sequenceIntegration()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
