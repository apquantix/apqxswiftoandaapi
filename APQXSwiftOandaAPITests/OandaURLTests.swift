//
//  OandaURLTests.swift
//  Wallet-ES
//
//  Created by Bonifatio Hartono on 3/31/16.
//  Copyright © 2016 Bonifatio Hartono. All rights reserved.
//

import XCTest

class OandaURLTests: XCTestCase {
    func x<T>(input: T) -> String{
        return String(reflecting: input).stringByReplacingOccurrencesOfString("APQXSwiftOandaAPITests.", withString: "")
    }
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measureBlock {
//            // Put the code you want to measure the time of here.
//        }
//    }

    func testURLComponents() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        /* Test Parameters */
        let environment : [OandaEnvironment] = [.FXTrade, .FXTradePractice]
        let basePollingPath : [OandaPollingBasePath] = [.Accounts, .Instruments, .Services]
        let baseStreamingPath : [OandaStreamingBasePath] = [.Prices, .Events]
        let basePollingSubPath: [OandaPollingSubPath] = [.Orders, .Trades, .Positions, .Transactions, .AllTransactions]

        
        /** Testing Polling Address */
        let pollingAddressTestResult : NSMutableArray = []
        _ = environment.map{ c in
            basePollingPath.map{ b in
                pollingAddressTestResult.addObject((NSURLComponents(environment: c, basePath: b).URL?.absoluteString)!)
                // function map
                print("\((NSURLComponents(environment: c, basePath: b).URL?.absoluteString)!) <- ", separator: "", terminator: "")
                print("NSURLComponents(environment: \(x(c)), basePath: \(x(b)))")
            }
        }
        let pollingAddressTestResultExpectation = [
            "https://api-fxtrade.oanda.com/v1/accounts",
            "https://api-fxtrade.oanda.com/v1/instruments",
            "https://api-fxtrade.oanda.com/v1/services",
            "https://api-fxpractice.oanda.com/v1/accounts",
            "https://api-fxpractice.oanda.com/v1/instruments",
            "https://api-fxpractice.oanda.com/v1/services"
        ]
        XCTAssertEqual(pollingAddressTestResult, pollingAddressTestResultExpectation)
        
        /** Test Streaming addresses */
        let streamingAddressTestResult : NSMutableArray = []
        _ = environment.map{ c in
            baseStreamingPath.map{ b in
                streamingAddressTestResult.addObject((NSURLComponents(environment: c, basePath: b).URL?.absoluteString)!)
                // function map
                print((NSURLComponents(environment: c, basePath: b).URL?.absoluteString)!, separator: "", terminator: "")
                print(" <-- NSURLComponents(environment: \(x(c)), basePath: \(x(b)))")
            }
        }
        let streamingAddressTestResultExpectation = [
            "https://stream-fxtrade.oanda.com/v1/prices",
            "https://stream-fxtrade.oanda.com/v1/events",
            "https://stream-fxpractice.oanda.com/v1/prices",
            "https://stream-fxpractice.oanda.com/v1/events"
        ]
        XCTAssertEqual(streamingAddressTestResult, streamingAddressTestResultExpectation)
        
        /** Test Account Information */
        let testAccountInfoAddress = NSURLComponents(environment: .FXTrade, accountID: "12345").URL?.absoluteString
        print(NSURLComponents(environment: OandaEnvironment.FXTrade, accountID: "12345").URL?.absoluteString, separator: "", terminator: "")
        print("NSURLComponents(environment: OandaEnvironment.FXTrade, accountID: '12345')")
        XCTAssertEqual(testAccountInfoAddress, "https://api-fxtrade.oanda.com/v1/accounts/12345")

        let testAccountInfoAddressPractice = NSURLComponents(environment: .FXTradePractice, accountID: "12345").URL?.absoluteString
        print(NSURLComponents(environment: OandaEnvironment.FXTradePractice, accountID: "12345").URL?.absoluteString, separator: "", terminator: "")
        print(" <-- NSURLComponents(environment: OandaEnvironment.FXTradePractice, accountID: '12345')")
        XCTAssertEqual(testAccountInfoAddressPractice, "https://api-fxpractice.oanda.com/v1/accounts/12345")
        
        
        /** Account Subpath */
        let testPollingSubPathResult : NSMutableArray = []
        _ = environment.map{ e in
            basePollingSubPath.map{ b in
                testPollingSubPathResult.addObject((NSURLComponents(environment: e, accountID: "12345", section: b).URL?.absoluteString)!)
                print((NSURLComponents(environment: e, accountID: "12345", section: b).URL?.absoluteString)!, separator: "", terminator: "")
                print(" <-- NSURLComponents(environment: \(x(e)), accountID: '12345', section: \(x(b)))")
            }
        }
        let testPollingSubPathResultExpectation = [
            "https://api-fxtrade.oanda.com/v1/accounts/12345/orders",
            "https://api-fxtrade.oanda.com/v1/accounts/12345/trades",
            "https://api-fxtrade.oanda.com/v1/accounts/12345/positions",
            "https://api-fxtrade.oanda.com/v1/accounts/12345/transactions",
            "https://api-fxtrade.oanda.com/v1/accounts/12345/alltransactions",
            "https://api-fxpractice.oanda.com/v1/accounts/12345/orders",
            "https://api-fxpractice.oanda.com/v1/accounts/12345/trades",
            "https://api-fxpractice.oanda.com/v1/accounts/12345/positions",
            "https://api-fxpractice.oanda.com/v1/accounts/12345/transactions",
            "https://api-fxpractice.oanda.com/v1/accounts/12345/alltransactions"
        ]
        XCTAssertEqual(testPollingSubPathResult, testPollingSubPathResultExpectation)
        
        /** Streaming account */
        let anURLItem : [NSURLQueryItem] = [NSURLQueryItem(name: "accountIds", value: "12345")]
        let testStreamingSubPathWithAccountResult : NSMutableArray = []
        _ = environment.map{ e in
            baseStreamingPath.map{ b in
                testStreamingSubPathWithAccountResult.addObject((NSURLComponents(environment: e, basePath: b, queryItems: anURLItem).URL?.absoluteString)!)
                print((NSURLComponents(environment: e, basePath: b, queryItems: anURLItem).URL?.absoluteString)!, separator: "", terminator: "")
                print(" <-- NSURLComponents(environment: \(x(e)), basePath: \(x(b)), queryItems: [NSURLQueryItem(name: 'accountIds', value: '12345')]")
            }
        }
        let testStreamingSubPathWithAccountResultExpectation = [
            "https://stream-fxtrade.oanda.com/v1/prices?accountIds=12345",
            "https://stream-fxtrade.oanda.com/v1/events?accountIds=12345",
            "https://stream-fxpractice.oanda.com/v1/prices?accountIds=12345",
            "https://stream-fxpractice.oanda.com/v1/events?accountIds=12345"
        ]
        XCTAssertEqual(testStreamingSubPathWithAccountResult, testStreamingSubPathWithAccountResultExpectation)
        
        /** Polling accounts */
        let testPollingSubPathWithAccountResult : NSMutableArray = []
        _ = environment.map{
            e in basePollingPath.map{
                b in testPollingSubPathWithAccountResult.addObject(
                    (NSURLComponents(environment: e, basePath: b, queryItems: anURLItem).URL?.absoluteString)!)
            }
        }
        let testPollingSubPathWithAccountResultExpectation = [
            "https://api-fxtrade.oanda.com/v1/accounts?accountIds=12345",
            "https://api-fxtrade.oanda.com/v1/instruments?accountIds=12345",
            "https://api-fxtrade.oanda.com/v1/services?accountIds=12345",
            "https://api-fxpractice.oanda.com/v1/accounts?accountIds=12345",
            "https://api-fxpractice.oanda.com/v1/instruments?accountIds=12345",
            "https://api-fxpractice.oanda.com/v1/services?accountIds=12345"
        ]
        XCTAssertEqual(testPollingSubPathWithAccountResult, testPollingSubPathWithAccountResultExpectation)
    }
}
