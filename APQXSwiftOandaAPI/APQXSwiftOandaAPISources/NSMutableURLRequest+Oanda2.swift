//
//  NSURLRequest+Oanda2.swift
//  APQXSwiftOandaAPI
//
//  Created by Bonifatio Hartono on 4/9/16.
//  Copyright © 2016 Bonifatio Hartono. All rights reserved.
//

import Foundation

// MARK: enum Oanda💡HeaderField
public enum Oanda💡HeaderField {
    case Authorization(token: String)
}
// MARK: enum Oanda💡Request
public enum Oanda💡Request {
    case Request(token: Oanda💡HeaderField, method: Oanda💡RequestMethod)
}

// MARK: enum Oanda💡RequestMethod
public enum Oanda💡RequestMethod {
    case GET(address: Oanda💡Host)
    case POST(data: Oanda💡Post)
    case PATCH(data: Oanda💡Patch)
    case DELETE(address: Oanda💡Host)
}
func xLoad(iNum : Oanda💡RequestMethod, x: NSMutableURLRequest){
    /* POST, PATCH and DELETE should transer Query Items to HTTP Body */
    switch iNum {
    case .GET(let address):
        x.HTTPMethod = "GET"
        x.URL = NSURLComponents(inHost: address).URL
        
    case .POST(let data): // Post case knows where to find it's own address.
        x.HTTPMethod = "POST"
        xLoad(data, x: x)
    case .PATCH(let data):
        x.HTTPMethod = "PATCH"        
        xLoad(data, x: x)
    case .DELETE(let address):
        x.HTTPMethod = "DELETE"
        let anURLComponents : NSURLComponents = NSURLComponents(inHost: address)
        x.URL = anURLComponents.URL
    }
}

// MARK: enum Oanda💡Post
public enum Oanda💡Post {
    case Orders(
        environment: Oanda💡Environment,
        accID : String,
        instrument: String,
        units: Int,
        side: Oanda💡PostOrderOptionSide,
        type: Oanda💡PostOrderOptionType,
        
        options: [Oanda💡PostOrderOption]
    )
}
func xLoad(iNum : Oanda💡Post, x: NSMutableURLRequest){
    switch iNum {
    case .Orders(let environment, let accID, let instrument, let units, let side, let type, let options):
        // Goes to order
        let address = Oanda💡Host.Address(.HTTPS, environment, .Poll(.Account(.Order(.ListDefault(accID: accID)))))
        let anURLComponent = NSURLComponents(inHost: address)
        var queryItems : [NSURLQueryItem] = []
        queryItems.append(NSURLQueryItem(name: "instrument", value: instrument))
        queryItems.append(NSURLQueryItem(name: "units", value: String(units)))
        queryItems.append(NSURLQueryItem(name: "side", value: side.rawValue))
        
        // Switching Type
        switch type { // Oanda💡PostOptionType
        case .Market :
            queryItems.append(NSURLQueryItem(name: "type", value: "market"))
        case .Limit(let price, let expiry):
            queryItems.append(NSURLQueryItem(name: "type", value: "limit"))
            queryItems.append(NSURLQueryItem(name: "price", value: String(price)))
            queryItems.append(NSURLQueryItem(name: "expiry", value: NSDateFormatter().customFormatter().stringFromDate(expiry) ))
        case .Stop(let price, let expiry):
            queryItems.append(NSURLQueryItem(name: "type", value: "limit"))
            queryItems.append(NSURLQueryItem(name: "price", value: String(price)))
            queryItems.append(NSURLQueryItem(name: "expiry", value: NSDateFormatter().customFormatter().stringFromDate(expiry) ))
        case .MarketIfTouched(let price, let expiry):
            queryItems.append(NSURLQueryItem(name: "type", value: "limit"))
            queryItems.append(NSURLQueryItem(name: "price", value: String(price)))
            queryItems.append(NSURLQueryItem(name: "expiry", value: NSDateFormatter().customFormatter().stringFromDate(expiry) ))
        }
        
        // processing options
        _ = options.map {
            switch $0 {
            case .LowerBound(let value):
                queryItems.append(NSURLQueryItem(name: "lowerBound", value: String(value)))
            case .UpperBound(let value):
                queryItems.append(NSURLQueryItem(name: "upperBound", value: String(value)))
            case .StopLoss(let value):
                queryItems.append(NSURLQueryItem(name: "stopLoss", value: String(value)))
            case .TakeProfit(let value):
                queryItems.append(NSURLQueryItem(name: "takeProfit", value: String(value)))
            case .TrailingStop(let value):
                queryItems.append(NSURLQueryItem(name: "trailingStop", value: String(value)))
            }
        }
        anURLComponent.queryItems = queryItems
        let theQueryItems = anURLComponent.query!
        
        anURLComponent.queryItems = nil
        x.URL = anURLComponent.URL
        x.HTTPBody = theQueryItems.dataUsingEncoding(NSUTF8StringEncoding)
    }
}
// MARK: enum Oanda💡PostOrderOptionSide : String
public enum Oanda💡PostOrderOptionSide : String {
    case Buy = "buy"
    case Sell = "sell"
}
// MARK: enum Oanda💡PostOrderOptionType
public enum Oanda💡PostOrderOptionType {
    case Limit(price: Float, expiry: NSDate)
    case Stop(price: Float, expiry: NSDate)
    case MarketIfTouched(price: Float, expiry: NSDate)
    case Market
}
// MARK: enum Oanda💡PostOrderOption
public enum Oanda💡PostOrderOption {
    /// The minimum execution price.
    case LowerBound(value: Float)
    /// The maximum execution price.
    case UpperBound(value: Float)
    /// The stop loss price.
    case StopLoss(value: Float)
    /// The take profit price.
    case TakeProfit(value: Float)
    /// The trailing stop distance in pips, up to one decimal place.
    case TrailingStop(value: Float)
}
// MARK: enum Oanda💡Patch
public enum Oanda💡Patch {
    case Orders(
        environment: Oanda💡Environment,
        accID : String,
        orderID : String,
        options: [Oanda💡PatchOrderOption] )
    case Trades(
        environment: Oanda💡Environment,
        accID : String,
        tradeID: String,
        options: [Oanda💡PatchTradeOption]
    )
}
func xLoad(iNum : Oanda💡Patch, x: NSMutableURLRequest){
    switch iNum {
    case .Orders(let environment, let accID, let orderID, let options):
        let address : Oanda💡Host = Oanda💡Host.Address(.HTTPS, environment, .Poll(.Account(.Order(.Information(accID: accID, orderID: orderID)))))
        let anURLComponent = NSURLComponents(inHost: address)
        
        if options.count > 0 {
            anURLComponent.queryItems =
                options.map {
                    switch $0 {
                    case .Units(let value):
                        return NSURLQueryItem(name: "units", value: String(value))
                    case .Price(let value):
                        return NSURLQueryItem(name: "price", value: String(value))
                    case .Expiry(let value):
                        return NSURLQueryItem(name: "expiry", value: NSDateFormatter().customFormatter().stringFromDate(value))
                    case .LowerBound(let value):
                        return NSURLQueryItem(name: "lowerBound", value: String(value))
                    case .UpperBound(let value):
                        return NSURLQueryItem(name: "upperBound", value: String(value))
                    case .StopLoss(let value):
                        return NSURLQueryItem(name: "stopLoss", value: String(value))
                    case .TakeProfit(let value):
                        return NSURLQueryItem(name: "takeProfit", value: String(value))
                    case .TrailingStop(let value):
                        return NSURLQueryItem(name: "trailingStop", value: String(value))
                    }
            }
            let theQueryItems = anURLComponent.query!
            
            anURLComponent.queryItems = nil
            x.URL = anURLComponent.URL
            x.HTTPBody = theQueryItems.dataUsingEncoding(NSUTF8StringEncoding)

        }
    case .Trades(let environment, let accID, let tradeID, let options):
        let address : Oanda💡Host =
            Oanda💡Host.Address(.HTTPS, environment, Oanda💡Type.Poll(.Account(.Trade(.Information(accID: accID, tradeID: tradeID)))))
        let anURLComponent = NSURLComponents(inHost: address)
        if options.count > 0 {
            anURLComponent.queryItems =
            options.map{
                switch $0 {
                case .StopLoss(let value):
                    return NSURLQueryItem(name: "stopLoss", value: String(value))
                case .TakeProfit(let value):
                    return NSURLQueryItem(name: "takeProfit", value: String(value))
                case .TrailingStop(let value):
                    return NSURLQueryItem(name: "trailingStop", value: String(value))
                }
            }
            let theQueryItems = anURLComponent.query!
            anURLComponent.queryItems = nil
            x.URL = anURLComponent.URL
            x.HTTPBody = theQueryItems.dataUsingEncoding(NSUTF8StringEncoding)

        }
        
        break
    }
}

// MARK: enum Oanda💡PatchTradeOption 
public enum Oanda💡PatchTradeOption  {
    case StopLoss(value :Float)
    case TakeProfit(value: Float)
    case TrailingStop(value: Float)
}

// MARK: enum Oanda💡PatchOrderOption
public enum Oanda💡PatchOrderOption {
    case Units(value: Int)
    case Price(value: Float)
    case Expiry(value: NSDate)
    case LowerBound(value: Float)
    case UpperBound(value: Float)
    case StopLoss(value: Float)
    case TakeProfit(value: Float)
    case TrailingStop(value: Float)
}


extension NSMutableURLRequest {
    
    /**
     This extension should help converting the NSURLComponents into NSURLRequest.
     This extension should also be able to convert the NSURLComponents Query Items
     into a POST Message body when requested.
     */
    public convenience init(oandaRequest: Oanda💡Request){
        self.init()
        switch oandaRequest {
        case .Request(let token, let method):
            switch token {
            case .Authorization(let token):
                self.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }
            xLoad(method, x: self)
        }
    }
}