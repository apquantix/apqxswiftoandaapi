//
//  NSMutableURLRequest+Oanda2.swift
//  APQXSwiftOandaAPI
//
//  Created by Bonifatio Hartono on 4/7/16.
//  Copyright © 2016 Bonifatio Hartono. All rights reserved.
//

import Foundation
import Cocoa

public enum Oanda💡Host {
    /// Host Address
    case Address(Oanda💡Scheme, Oanda💡Environment, Oanda💡Type)
}
public enum Oanda💡Scheme {
    /// HTTPS Connection
    case HTTPS
}
public enum Oanda💡Type {
    /// Streaming Connection
    case Stream(Oanda💡StreamingPath)
    /// Polling Connection
    case Poll(Oanda💡Path)
}
public enum Oanda💡Environment {
    /// Real Account
    case FXTrade
    /// Practice Account
    case FXTradePractice
}

public enum Oanda💡StreamingPath {

    
    /// Path to stream the prices.. 
    /// /v1/prices?accountId=123&instruments=EUR_USD
    case DefaultPriceList(accID: String, instrument: String)
    /// Path to stream events.. /v1/events
    case DefaultEvents
    
    /// Path to stream events.. /v1/events?accountIds=12345
    case Events(accountIds: String)
    /// Path to stream the prices.. 
    /// /v1/prices?accountId=123&instruments=EUR_USD&sessionId=xxxx
    case PriceList(accID: String, instrument: String, sessionID: String)
    
}

public enum Oanda💡Path {
    /// Path to retrieve account list. /v1/accounts
    case Account(Oanda💡Account)
    /// Path to retrieve instrument list. /v1/instruments
    case Instrument(Oanda💡Instrument)
    /// Path to retrieve Oanda Services. /v1/services
    case Services
    /// Path to retrieve Oanda Prices. /v1/prices
    case Prices
    /// Path to retrieve Oanda Instrument history candles. /v1/candles
    case Candles
    /// Path to retrieve Oanda Instrument history candles. /v1/candles
    case DefaultCandles(instrument: String)
    
    /**
     FXLabs Calendar
     
     [More Info]:
     http://developer.oanda.com/rest-live/forex-labs/#calendar
     [More Info]
	*/
    case Calendar(
        period: Oanda💡CalendarPeriodOption,
        option: [Oanda💡CalendarOption]
    )
    
    /**
     Forex Labs 1 Year worth of historical positions.
     
     [More Info]:
     http://developer.oanda.com/rest-live/forex-labs/#historical_position_ratios
     [More Info]
	*/
    case HistoricalPositionRatios(
        Oanda💡HPROptionInstrument,
        Oanda💡HPROptionPeriod
    )
    /**
     Forex Labs 1 Year worth of spread information.
     
     [More Info]:
     http://developer.oanda.com/rest-live/forex-labs/#spreads
     [More Info]
     */
    case Spreads(
        instrument: String,
        period: Oanda💡SpreadOptionPeriod)
    
    case CommitmentsOfTraders(
        instrument: Oanda💡CommitmentsOfTradersOptionInstrument)
    
    /**
     Forex Labs 1 Year worth of OANDA Order book data.
     
     [More Info]:
     http://developer.oanda.com/rest-live/forex-labs/#spreads
     [More Info]
     */
    case OrderBook(
        instrument: Oanda💡OrderBookOptionInstrument,
        period: Oanda💡OrderBookOptionPeriod)
    
    case AutoChartList(option: [Oanda💡AutoChartListOption])
}
public enum Oanda💡AutoChartListOption {
    case Instrument(String)
    case Period(Oanda💡AutoChartListOptionPeriod)
    case Quality(Oanda💡AutoChartListOptionQuality)
    case Type(Oanda💡AutoChartListOptionType)
    case Direction(Oanda💡AutoChartListOptionDirection)
}
public enum Oanda💡AutoChartListOptionPeriod : String{
    case FourHours      = "14400"
    case EightHours     = "28800"
    case TwelveHours    = "43200"
    case OneDay         = "86400"
    case OneWeek        = "604800"
}
public enum Oanda💡AutoChartListOptionQuality{
    case One, Two, Three, Four, Five, Six, Seven, Nine, Ten
}
public enum Oanda💡AutoChartListOptionType : String{
    case chartpattern, keylevel
}
public enum Oanda💡AutoChartListOptionDirection : String {
    case bullish, bearish
}

public enum Oanda💡OrderBookOptionInstrument : String{
    case
    AUD_JPY, AUD_USD, EUR_AUD, EUR_CHF, EUR_GBP,
    EUR_JPY, EUR_USD, GBP_CHF, GBP_JPY, GBP_USD,
    NZD_USD, USD_CAD, USD_CHF, USD_JPY, XAU_USD,
    XAG_USD
}

public enum Oanda💡OrderBookOptionPeriod : String{
    ///    3600 - 1 hour - 20 minute snapshots
    case OneHour = "3600"
    ///    21600 - 6 hour - 20 minute snapshots
    case SixHours = "21600"
    ///    86400 - 1 day - 2 hour snapshots
    case OneDay = "86400"
    ///    604800 - 1 week - two daily snapshots at 04:00 and 16:00 GMT
    case OneWeek = "604800"
    ///    2592000 - 1 month - 1 day snapshots at 16:00 GMT
    case OneMonth = "2592000"
    ///    31536000 - 1 year - 1 month snapshots first of the month at 12:00 GMT
    case OneYear = "31536000"

    
}
public enum Oanda💡CommitmentsOfTradersOptionInstrument : String {
    case
    AUD_USD, GBP_USD, USD_CAD, EUR_USD, USD_JPY,
    USD_MXN, NZD_USD, USD_CHF, XAU_USD, XAG_USD
}
public enum Oanda💡SpreadOptionPeriod : String{
    case OneHour        = "3600"
    case TwelveHours    = "43200"
    case OneDay         = "86400"
    case OneWeek        = "604800"
    case OneMonth       = "2592000"
    case ThreeMonths    = "7776000"
    case SixMonths      = "15552000"
    case OneYear        = "31536000"
}

public enum Oanda💡HPROptionInstrument : String {
    case
    AUD_JPY, AUD_USD, EUR_AUD, EUR_CHF, EUR_GBP,
    EUR_JPY, EUR_USD, GBP_CHF, GBP_JPY, GBP_USD,
    NZD_USD, USD_CAD, USD_CHF, USD_JPY, XAU_USD,
    XAG_USD
}
public enum Oanda💡HPROptionPeriod : String {
    case OneDay = "86400"
    case TwoDay = "172800"
    case OneWeek = "604800"
    case OneMonth = "2592000"
    case ThreeMonths = "7776000"
    case SixMonths = "15552000"
    case OneYear = "31536000"
}

public enum Oanda💡CalendarOption{
    case Instrument(String)
}
public enum Oanda💡CalendarPeriodOption : String{
    /// 1 hour = 3,600 seconds
    case OneHour = "3600"
    /// 1 day = 43,200 seconds
    case TwelveHours = "43200"
    /// 1 day = 86,400 seconds
    case OneDay = "86400"
    /// 1 week = 604,800 seconds
    case OneWeek = "604800"
    /// 1 month = 2,592,000 seconds
    case OneMonth = "2592000"
    /// 3 months = 7,776,000 seconds
    case ThreeMonths = "7776000"
    /// 6 months = 15,552,000 seconds
    case SixMonths = "15552000"
    /// 1 year = 31,536,000 seconds
    case OneYear = "31536000"
}
func xLoad(iNum: Oanda💡CalendarPeriodOption, x: NSURLComponents){
    switch iNum{
    case .OneHour       :
        x.queryItems = [NSURLQueryItem(name: "period", value: "3600")]
    case .TwelveHours   :
        x.queryItems = [NSURLQueryItem(name: "period", value: "43200")]
    case .OneDay        :
        x.queryItems = [NSURLQueryItem(name: "period", value: "86400")]
    case .OneWeek       :
        x.queryItems = [NSURLQueryItem(name: "period", value: "604800")]
    case .OneMonth      :
        x.queryItems = [NSURLQueryItem(name: "period", value: "2592000")]
    case .ThreeMonths   :
        x.queryItems = [NSURLQueryItem(name: "period", value: "7776000")]
    case .SixMonths     :
        x.queryItems = [NSURLQueryItem(name: "period", value: "15552000")]
    case .OneYear       :
        x.queryItems = [NSURLQueryItem(name: "period", value: "31536000")]
    }
}
public enum Oanda💡Instrument {
    /// Path to retrieve instrument list. /v1/instrument
    /// FIXME: can't have more than one field or one instrument
    case List(
        accID: String,
        fields:[Oanda💡InstrumentField],
        instruments:[String])
    /// Path to retrieve instrument list. /v1/instrument?accountId=:accountId
    case DefaultList(accID: String)
}
func xLoad(iNum : Oanda💡Instrument, x: NSURLComponents){
    switch iNum{
    case  .DefaultList(let accID)                           :
        x.queryItems = [NSURLQueryItem(name: "accountId", value: accID)]
    case  .List(let accID, let fields, let instruments)     :
        x.queryItems = [NSURLQueryItem(name: "accountId", value: accID)]
        if fields.count > 0 { break }
        if instruments.count > 0 { break }
    }
}

/**
    [More Info]:
        http://developer.oanda.com/rest-live/rates/#getInstrumentList
    [More Info]
 */
public enum Oanda💡InstrumentField{
    /// Name of the instrument. This value should be used to fetch prices and create orders and trades.
    case Instrument
    /// Display name for the end user.
    case DisplayName
    /// Value of 1 pip for the instrument.
    case PIP
    /// The maximum number of units that can be traded for the instrument.
    case MaxTradeUnits
    /// The smallest unit of measurement to express the change in value between the instrument pair.
    case Precision
    case MaxTrailingStop
    case MinTrailingStop
    case MarginRate
    case Halted
    case InterestRate
    
}
public enum Oanda💡Trade{
    /** Get list of open trades.
     GET /v1/accounts/:account_id/trades
     */
    case List(accID: String, options: [Oanda💡TradeOption])
    /** Get information on a specific trade
     GET /v1/accounts/:account_id/trades/:trade_id
     */
    case Information(accID: String, tradeID: String)
}


public enum Oanda💡TradeOption {
    /** The server will return trades with id less than or equal to this,
        in descending order (for pagination).
     */
    case MaxId(String)
    /** Maximum number of open trades to return. Default: 50 Max value: 500 */
    case Count(String)
    /** Retrieve open trades for a specific instrument only Default: all */
    case Instrument(String)
    /** A (URL encoded) comma separated list of trades to retrieve. 
     Maximum number of ids: 50.
     No other parameter may be specified with the ids parameter.
     */
    case Ids(String)
}
public enum Oanda💡Account {
    /// Path to retrieve account list. /v1/accounts
    case List
    /// Path to retrieve account information. /v1/accounts/:accountID
    case Information(accID: String)
    /// Path to retrieve account list. /v1/accounts/:accountID/orders
    @available(*, unavailable)
    case Orders
    /// Path to retrieve account list. /v1/accounts/:accountID/orders
    case Order(Oanda💡Order)
    /// sub path to retrieve account's trades. /v1/accounts/:accountID/trades
    @available(*, unavailable)
    case Trades
    /// sub path to retrieve account's trades. /v1/accounts/:accountID/trades
    case Trade(Oanda💡Trade)
    /// sub path to retrieve account's positions /v1/accounts/:accountID/positions
    @available(*, unavailable)
    case Positions
    /// sub path to retrieve account's positions /v1/accounts/:accountID/positions
    case Position(Oanda💡Position)
    /// sub path to retrieve account's transactions /v1/accounts/:accountID/transactions
    @available(*, unavailable)
    case Transactions
    /// sub path to retrieve account's transactions /v1/accounts/:accountID/transactions
    case Transaction(Oanda💡Transaction)
    /// sub path to retrieve all transactions /v1/accounts/:accountID/alltransactions
    @available(*, unavailable)    
    case AllTransactions
}

public enum Oanda💡TransactionOption {
    /** The first transaction to get.
    The server will return transactions with id less than or equal to this, in descending order
     */
    case MaxId(String)
    /**The last transaction to get. 
     The server will return transactions with id greater or equal to this, in descending order.
     */
    case MinId(String)
    
    /**The maximum number of transactions to return.
     The maximum value that can be specified is 500.
     By default, if count is not specified, a maximum of 50 transactions will be fetched.
     Note: Transactions requests with the count parameter specified is rate limited to 1 per every 60 seconds.
     */
    case Count(String)
    /**Retrieve transactions for a specific instrument only.
	Default: all.
	*/
    case Instrument(String)
    /**An URL encoded comma (%2C) separated list of transaction ids to retrieve.
	Maximum number of ids: 50.
	No other parameter may be specified with the ids parameter.
	*/
    case Ids(String)
}
func xLoad(
    iNum: Oanda💡TransactionOption,
    x: NSURLComponents)
    -> NSURLQueryItem {
    switch iNum {
    case .Count(let inStr):
        return NSURLQueryItem(name: "count", value: inStr)
    case .Ids(let inStr):
        return NSURLQueryItem(name: "ids", value: inStr)
    case .Instrument(let inStr):
        return NSURLQueryItem(name: "instrument", value: inStr)
    case .MaxId(let inStr):
        return NSURLQueryItem(name: "maxId", value: inStr)
    case .MinId(let inStr):
        return NSURLQueryItem(name: "minId", value: inStr)
    }
}

public enum Oanda💡Transaction {
    /// Get transaction history
    case History(accID: String, options: [Oanda💡TransactionOption])
    case Information(accID: String, transactionID: String)
    case AllTransaction(accID: String)
}
func xLoad(iNum: Oanda💡Transaction, x : NSURLComponents){
    switch iNum {
    case .History(let accID, let options):
        x.path = "/v1/accounts/\(accID)/transactions"
        if options.count > 0 {
            x.queryItems =
            options.map { xLoad($0, x: x)}
        }
    case .Information(let accID, let transactionID):
        x.path = "/v1/accounts/\(accID)/transactions/\(transactionID)"
    case .AllTransaction(let accID):
        x.path = "/v1/accounts/\(accID)/alltransactions"
    }
}

public enum Oanda💡Position{
    /// Get a list of all open positions
    case List(accID: String)
    /// Get the position for an instrument can also be used for Delete
    case Information(accID: String, instrumentID: String)
}
func xLoad(iNum: Oanda💡Position, x : NSURLComponents){
    switch iNum {
    case .List(let accID):
        x.path = "/v1/accounts/\(accID)/positions"
    case .Information(let accID, let instrumentID):
        x.path = "/v1/accounts/\(accID)/positions/\(instrumentID)"
    }
}

public enum Oanda💡Order {
    /// Path to retrieve orders list. /v1/accounts/:accountID/orders
    case ListDefault(accID: String)
    /// Path to retrieve orders list. /v1/accounts/:accountID/orders
    case List(
        accID       : String,
        maxID       : String,
        count       : String,
        instrument  : String,
        ids         : String
    )
    /// Path to retrieve orders
    case Information(accID: String, orderID: String)

}

extension NSURLComponents {
    convenience init(inHost: Oanda💡Host) {
        self.init()
        let s = self
        switch inHost {
        case .Address(let inScheme, let inEnvironment, let inType):
            switch inScheme {
            case .HTTPS: s.scheme = "https"
            }
            switch inType{
            case .Stream(let streamPath):
                self.host = "stream-"
                switch streamPath {
                case .DefaultEvents: self.path = "/v1/events"
                case .DefaultPriceList(let accID, let instrument) :
                    self.path = "/v1/prices"
                    self.queryItems = [
                        NSURLQueryItem(name: "accountId", value: accID),
                        NSURLQueryItem(name: "instruments", value: instrument),
                    ]
                case let .Events(accountIds ) :
                    self.path = "/v1/events"
                    self.queryItems = [
                        NSURLQueryItem(name: "accountIds", value: accountIds)
                    ]
                case let .PriceList(accID , instrument , sessionID ):
                    self.path = "/v1/prices"
                    self.queryItems = [
                        NSURLQueryItem(name: "accountId", value: accID),
                        NSURLQueryItem(name: "instruments", value: instrument),
                        NSURLQueryItem(name: "sessionId", value: sessionID)
                    ]
                }
            case .Poll(let inPath):
                self.host = "api-"
                switch inPath {
                case .Account(let inAccount) :
                    switch inAccount {
                    case .AllTransactions: break
                    case .Information(let accID):
                        self.path = "/v1/accounts/\(accID)"
                    case .List: self.path = "/v1/accounts"
                    case .Order(let inOrder):
                        switch inOrder {
                        case .ListDefault(let accID):
                            self.path = "/v1/accounts/\(accID)/orders"
                        case let .List(accID, maxID, count, instrument, ids):
                            self.path = "/v1/accounts/\(accID)/orders"
                            self.queryItems = [
                                NSURLQueryItem(name: "maxId", value: maxID),
                                NSURLQueryItem(name: "count", value: count),
                                NSURLQueryItem(name: "instrument", value: instrument),
                                NSURLQueryItem(name: "ids", value: ids)
                            ]
                        case let .Information(accID, orderID) :
                            self.path = "/v1/accounts/\(accID)/orders/\(orderID)"
                        }
                    case .Orders : break
                    case .Positions: break
                    case .Trades: break
                    case .Trade(let inTrade) :
                        switch inTrade {
                        case .List(let accID, let options) :
                            self.path = "/v1/accounts/\(accID)/trades"
                            if (options.count > 0) {
                                self.queryItems =
                                    options.map{
                                        switch $0 {
                                        case .MaxId(let inString) :
                                            return NSURLQueryItem(name: "maxId", value: inString)
                                        case .Count(let inString) :
                                            return NSURLQueryItem(name: "count", value: inString)
                                        case .Ids(let inString) :
                                            return NSURLQueryItem(name: "instrument", value: inString)
                                        case .Instrument(let inString):
                                            return NSURLQueryItem(name: "ids", value: inString)
                                        }
                                }
                            }
                        case .Information(let accID, let tradeID):
                            self.path = "/v1/accounts/\(accID)/trades/\(tradeID)"
                        }
                    case .Transactions: break
                    case .Position(let iNum):
                        xLoad(iNum, x: self)
                    case .Transaction(let iNum):
                        xLoad(iNum, x: self)
                    }
                case .Candles       : self.path = "/v1/candles"
                case .DefaultCandles(let instrument) :
                    self.path = "/v1/candles"
                    self.queryItems = [NSURLQueryItem(name: "instrument", value: instrument)]
                case .Instrument(let inOption)   :
                    self.path = "/v1/instruments"
                    xLoad(inOption, x: self)
                case .Prices        : self.path = "/v1/prices"
                case .Services      : self.path = "/v1/services"
                case .Calendar(let period, let option):
                    self.path = "/labs/v1/calendar"
                    xLoad(period, x: self)
                    _ = option.map {
                        switch $0 {
                        case .Instrument(let inString) :
                            self.queryItems!.append(NSURLQueryItem(name: "instrument", value: inString))
                        }
                    }
                    
                case .HistoricalPositionRatios(let inInstrument, let inPeriod):
                    self.path = "/labs/v1/historical_position_ratios"
                    self.queryItems = []
                    switch inInstrument {
                    default:
                        self.queryItems!.append(NSURLQueryItem(name: "instrument", value: inInstrument.rawValue))
                    }
                    switch inPeriod{
                    default:
                        self.queryItems!.append(NSURLQueryItem(name: "period", value: inPeriod.rawValue))
                    }
                case .Spreads(let instrument, let period):
                    self.path = "/labs/v1/spreads"
                    self.queryItems = []
                    self.queryItems!.append(NSURLQueryItem(name: "instrument", value: instrument))
                    switch period{
                    default:
                        self.queryItems!.append(NSURLQueryItem(name: "period", value: period.rawValue))
                    }
                case .CommitmentsOfTraders(let instrument):
                    self.path = "/labs/v1/commitments_of_traders"
                    self.queryItems = []
                    switch instrument {
                    default:
                        self.queryItems!.append(NSURLQueryItem(name: "instrument", value: instrument.rawValue))
                        
                    }
                case .OrderBook(let instrument, let period) :
                    self.path = "/labs/v1/orderbook_data"
                    self.queryItems = []
                    switch instrument {
                    default:
                        self.queryItems!.append(NSURLQueryItem(name: "instrument", value: instrument.rawValue))
                    }
                    switch period {
                    default:
                        self.queryItems!.append(NSURLQueryItem(name: "period", value: period.rawValue))
                    }
                case .AutoChartList(let option):
                    self.path = "/labs/v1/signal/autochartist"
                    if option.count > 0 {
                    self.queryItems =
                        option.map {
                            switch $0 {
                            case .Direction(let inDirection):
                                switch inDirection {
                                case .bearish : return NSURLQueryItem(name: "direction", value: "bearish" )
                                case .bullish : return NSURLQueryItem(name: "direction", value: "bullish" )
                                }
                            case .Instrument(let inString):
                                return NSURLQueryItem(name: "instrument", value: inString)
                            case .Period(let period):
                                switch period {
                                default:
                                    return NSURLQueryItem(name: "period", value: period.rawValue)
                                }
                            case .Quality(let inQuality):
                                switch inQuality{
                                default:
                                    return NSURLQueryItem(name: "quality", value: "\(inQuality.hashValue + 1)")
                                }
                            case .Type(let aType):
                                switch aType {
                                default :
                                    return NSURLQueryItem(name: "type", value: aType.rawValue)
                                }
                            }
                        }
                    }
                }
                
            }
            switch inEnvironment {
            case .FXTrade: self.host = "\(self.host!)fxtrade.oanda.com"
            case .FXTradePractice: self.host = "\(self.host!)fxpractice.oanda.com"
            }
        }
    }
}