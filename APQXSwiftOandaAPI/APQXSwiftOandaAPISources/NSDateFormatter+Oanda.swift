//
//  NSDateFormatter+Oanda.swift
//  APQXSwiftOandaAPI
//
//  Created by Bonifatio Hartono on 4/10/16.
//  Copyright © 2016 Bonifatio Hartono. All rights reserved.
//

import Foundation

extension NSDateFormatter {
    func customFormatter() -> NSDateFormatter{
        let aPOSIXLocale = NSLocale(localeIdentifier: "en_US_POSIX")
        self.locale = aPOSIXLocale
        self.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z"
        self.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        return self
    }
}
